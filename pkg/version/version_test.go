package version

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

// Sample test
func TestVersion(t *testing.T) {
	assert.Equal(t, Version, "UNKNOWN")
}